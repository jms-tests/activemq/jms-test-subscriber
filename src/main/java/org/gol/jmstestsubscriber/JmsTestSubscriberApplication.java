package org.gol.jmstestsubscriber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmsTestSubscriberApplication {

    public static void main(String[] args) {
        SpringApplication.run(JmsTestSubscriberApplication.class, args);
    }

}
