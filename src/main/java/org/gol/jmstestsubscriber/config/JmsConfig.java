package org.gol.jmstestsubscriber.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;

import javax.jms.ConnectionFactory;

@Configuration
public class JmsConfig {

    public static final String QUEUE_FACTORY = "queueListenerFactory";
    public static final String TOPIC_FACTORY = "topicListenerFactory";
    public static final String DURABLE_TOPIC_FACTORY_A = "durableTopicListenerFactoryA";
    public static final String DURABLE_TOPIC_FACTORY_B = "durableTopicListenerFactoryB";

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Value("${spring.activemq.user}")
    private String userName;

    @Value("${spring.activemq.password")
    private String userPassword;



    @Bean(QUEUE_FACTORY)
    public JmsListenerContainerFactory queueListenerFactory(
            ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setPubSubDomain(false);
        return factory;
    }

    @Bean(TOPIC_FACTORY)
    public JmsListenerContainerFactory topicListenerFactory(
            ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setPubSubDomain(true);
        return factory;
    }

    @Bean(DURABLE_TOPIC_FACTORY_A)
    public JmsListenerContainerFactory durableTopicListenerFactoryA(DefaultJmsListenerContainerFactoryConfigurer configurer) {
        return createDurableTopicListenerFactory("durable-subscriber-A", configurer);
    }

    @Bean(DURABLE_TOPIC_FACTORY_B)
    public JmsListenerContainerFactory durableTopicListenerFactoryB(DefaultJmsListenerContainerFactoryConfigurer configurer) {
        return createDurableTopicListenerFactory("durable-subscriber-B", configurer);
    }

    private JmsListenerContainerFactory createDurableTopicListenerFactory(
            String clientId,
            DefaultJmsListenerContainerFactoryConfigurer configurer) {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(userName, userPassword, brokerUrl);
        connectionFactory.setClientID(clientId);

        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setPubSubDomain(true);
        factory.setSubscriptionDurable(true);
        return factory;
    }
}
